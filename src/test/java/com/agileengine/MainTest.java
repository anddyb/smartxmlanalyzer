package com.agileengine;


import com.agileengine.core.Analyzer;
import com.agileengine.core.Context;
import com.agileengine.util.JsoupUtils;
import org.jsoup.nodes.Element;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


/**
 * Unit test for Main.
 */
public class MainTest {

    private String originFilePath;

    private Map<String, String> testFilesPaths;

    @Before
    public void initTestSamples() {

        originFilePath = "sample-0-origin.html";

        testFilesPaths = new HashMap<>();

        testFilesPaths.put("sample-1-evil-gemini.html",
                "#document > html > body[1] > div > div[1] > div[2] > div > div > div[1] > a");
        testFilesPaths.put("sample-2-container-and-clone.html",
                "#document > html > body[1] > div > div[1] > div[2] > div > div > div[1] > div > a");
        testFilesPaths.put("sample-3-the-escape.html",
                "#document > html > body[1] > div > div[1] > div[2] > div > div > div[2] > a");
        testFilesPaths.put("sample-4-the-mash.html",
                "#document > html > body[1] > div > div[1] > div[2] > div > div > div[2] > a");

    }

    @Test
    public void testSampleFiles() throws IOException {

        final String path = this.getClass().getClassLoader().getResource("").getPath();

        for (Map.Entry<String, String> testEntry : testFilesPaths.entrySet()) {

            final Element actualResult = new Analyzer(Context.init(path + originFilePath, path + testEntry.getKey()),
                    JsoupUtils::findSimilar).findSimilar();

            Assert.assertEquals(testEntry.getValue(), JsoupUtils.getPath(actualResult));

        }

    }

}
