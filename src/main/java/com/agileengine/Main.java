package com.agileengine;


import com.agileengine.core.Analyzer;
import com.agileengine.core.Context;
import com.agileengine.util.JsoupUtils;
import org.apache.log4j.Logger;
import org.jsoup.nodes.Element;

import java.io.IOException;


/**
 * Main
 *
 */
public class Main {

    private static final Logger LOG = Logger.getLogger(Main.class);

    public static void main(String[] args) {

        final Context context = Context.init(args);

        LOG.info("Context : " + context);

        try {

            final Analyzer analyzer = new Analyzer(context, JsoupUtils::findSimilar);

            LOG.info("Starting analyzer...");

            final Element result = analyzer.findSimilar();

            if (result == null) {
                LOG.info("Similar element had not been found.");
            } else {
                LOG.info("The most similar element : " + JsoupUtils.getPath(result));
            }

        } catch (IOException e) {
            LOG.error("Error while analyzing files.", e);
        }

    }


}
