package com.agileengine.core;


import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;


/**
 * Implementing this interface allows an object to search for the similar
 * elements based on some reference element.
 *
 */
public interface Searcher {

    /**
     * Performs the search of element the most similar to the provided one.
     *
     * @param document The document with target elements.
     * @param originElement The origin element.
     * @return a {@code Element} the most similar to the origin one.
     */
    Element search(Document document, Element originElement);

}
