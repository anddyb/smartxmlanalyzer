package com.agileengine.core;


import com.agileengine.util.JsoupUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;


/**
 * An {@link Analyzer} instance processes a reference file and
 * based on received data, it looks for a similar elements in the another file.
 * The search performs according to the provided strategy {@link Searcher}.
 *
 */
public class Analyzer {

    private final Context context;

    private final Searcher searcher;

    public Analyzer(Context context, Searcher searcher) {
        this.context = context;
        this.searcher = searcher;
    }

    /**
     * Find similar element.
     *
     * @return the element
     * @throws IOException the io exception
     */
    public Element findSimilar() throws IOException {

        final Element referenceElement = JsoupUtils.getDocument(context.getOriginFilePath())
                .getElementById(context.getInitialTargetElementId());

        Element result;

        if (referenceElement == null) {

            result = null;

        } else {

            final Document newDocument = JsoupUtils.getDocument(context.getNewFilePath());

            result = searcher.search(newDocument, referenceElement);

        }

        return result;

    }

}
