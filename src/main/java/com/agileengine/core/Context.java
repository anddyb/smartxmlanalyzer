package com.agileengine.core;


public final class Context {

    private static final String INITIAL_TARGET_ELEMENT_ID_DEFAULT_VALUE = "make-everything-ok-button";

    private final String originFilePath;

    private final String newFilePath;

    private final String initialTargetElementId;

    private Context(String originFilePath, String inputOtherSampleFilePath, String initialTargetElementId) {
        this.originFilePath = originFilePath;
        this.newFilePath = inputOtherSampleFilePath;
        this.initialTargetElementId = initialTargetElementId;
    }

    public static Context init(String... args) {

        final int length = args.length;

        if (length < 2) {
            throw new IllegalArgumentException("Error while initializing app context, "
                    + "mandatory parameter(s) are missing.");
        }

        final String initialTargetElementId = (length == 2 ? INITIAL_TARGET_ELEMENT_ID_DEFAULT_VALUE : args[2]);

        return new Context(args[0], args[1], initialTargetElementId);
    }

    public String getOriginFilePath() {
        return originFilePath;
    }

    public String getNewFilePath() {
        return newFilePath;
    }

    public String getInitialTargetElementId() {
        return initialTargetElementId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{");
        sb.append("originFilePath='").append(originFilePath).append('\'');
        sb.append(", newFilePath='").append(newFilePath).append('\'');
        sb.append(", initialTargetElementId='").append(initialTargetElementId).append('\'');
        sb.append('}');
        return sb.toString();
    }

}
