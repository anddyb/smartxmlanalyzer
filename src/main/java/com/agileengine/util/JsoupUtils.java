package com.agileengine.util;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;


/**
 *  This class contains various methods for manipulating Jsoup
 *  documents, elements.
 */
public final class JsoupUtils {

    private final static List<String> ATTRIBUTES_BY_PRIORITY
            = Arrays.asList("id", "onclick", "href", "title");

    private JsoupUtils() {
    }

    public static Document getDocument(String filePath) throws IOException {
        return Jsoup.parse(new File(filePath), StandardCharsets.UTF_8.displayName());
    }

    public static Element findSimilar(Document document, Element originElement) {

        for (String attributeKey : ATTRIBUTES_BY_PRIORITY) {

            final String originValue = originElement.attr(attributeKey);

            if (!originValue.isEmpty()) {

                final Elements elements = selectByAttr(document, attributeKey, originValue);
                if (elements.size() == 1) {
                    return elements.get(0);
                }

            }

        }

        return null;

    }

    private static Elements selectByAttr(Document doc, String key, String value) {
        return doc.select("[" + key + "=" + value + "]");
    }

    public static String getPath(Element targetElement) {

        String path = targetElement.nodeName();
        Element element = targetElement;

        while ((element = element.parent()) != null) {

            final Integer elementSiblingIndex = element.elementSiblingIndex();
            final String index = elementSiblingIndex == 0 ? "" : "[" + elementSiblingIndex + "]";

            path = element.nodeName() + index + " > " + path;

        }

        return path;
    }

}
